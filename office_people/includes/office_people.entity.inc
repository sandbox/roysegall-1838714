<?php

/**
 * @file
 * Entity class for office people module.
 */
class OfficePeopleShiftEntity extends EntityAPIController {

  /**
   * The shift ID.
   */
  public $sid;

  /**
   * The shift preventive title.
   */
  public $title;

  /**
   * Unix time stamp for the start of the shift.
   */
  public $start_time;

  /**
   * The hard coded unix time stamp the shift started. The end time can be
   * start change to any time but the hard coded time will keep the same value
   * for ever.
   */
  public $start_time_hard;

  /**
   * The unix time stamp the shift ended at.
   */
  public $end_time;

  /**
   * The hard coded unix time stamp the shift ended at.
   *
   * @see $this->start_time_hard.
   */
  public $end_time_hard;

  /**
   * The user UID which own the shift.
   */
  public $uid;


  /**
   * Overrides create().
   */
  public function create(array $values = array()) {
    parent::create($values);

    $user = office_user_init();
    $values += array(
      'title' => '',
      'uid' => $user->uid,
      'bundle' => 'shift',
      'start_time' => time(),
      'start_time_hard' => time(),
    );

    foreach ($values as $key => $value) {
      $this->{$key} = $value;
    }

    $this->shiftTitle($this);

    return $this;
  }

  /**
   * Overrides save().
   */
  public function save($entity = NULL) {
    if (!is_object($entity)) {
      $this->sid = empty($this->sid) ? 0 : $this->sid;
      $entity = $this;
    }

    if (empty($this->end_time_hard) && !empty($this->end_time)) {
      // If the end time is set but not the hard end time is not set - set it.
      $this->end_time_hard = $this->end_time;
    }
    else if (!empty($this->end_time_hard) && empty($this->end_time)) {
      // If the hard end time is set but not the end time is not set - set it.
      $this->end_time = $this->end_time_hard;
    }

    // Building the entity label.
    $this->shiftTitle($entity);

    // Write the entity to the table.
    $primary_keys = $entity->sid ? 'sid' : array();
    drupal_write_record('shifts', $entity, $primary_keys);

    // Let other module to know that we are saving the entity.
    module_invoke_all('entity_presave', $entity, 'shift');
    module_invoke_all('shift_presave', $entity);

    if (empty($primary_keys)) {
      field_attach_insert('shift', $entity);
      module_invoke_all('entity_save', $entity);
    }
    else {
      field_attach_update('shift', $entity);
      module_invoke_all('entity_update', $entity, 'shift');
    }

    return $this;
  }

  /**
   * Overrides the basic load function. Gathering first the object from the DB
   * ad then wrapp them with the class methods. This will help us use the code
   * in the next way:
   *
   *  $shift = entity_load('shift', ID);
   *  $shift->start_time = time();
   *  $shift->save();
   */
  public function load($ids = array(), $condition = array()) {
    if (!entity_access('view', 'shift')) {
      return array();
    }
    // During a hook_init() call the $ids can changed to object and we will get
    // a fatal error.
    foreach ($ids as $id) {
      if (is_array($id)) {
        return array();
      }
    }
    // Load all of the entities from the DB and then build an array of them with
    // the class methods.
    $entities = parent::load($ids, $condition);

    foreach ($entities as &$entity) {
      $dummy = clone $this;
      // Run over the variagles and add it to the $dummy object.
      foreach (get_object_vars($entity) as $key => $value) {
        $dummy->{$key} = $value;
      }
      $entity = $dummy;
    }

    return $entities;
  }

  /**
   * Close shift.
   */
  function close($time = NULL) {
    $time = !empty($time) ? $time : time();
    $this->end_time = $this->end_time_hard = $time;
    $this->save();
  }

  /**
   * Check if the shift close.
   */
  function shiftClose() {
    return $this->_shiftStatus('close');
  }

  /**
   * Check of the shift is open.
   */
  function shiftOpen() {
    return $this->_shiftStatus('open');
  }

  /**
   * Helper function for the shift status - cloe/open.
   *
   *  @param $askedStatus
   *    What the status you want to check - available values:
   *      - close
   *      - open
   * @return bool
   *  Return true or false of the the shift has already started.
   */
  function _shiftStatus($askedStatus) {
    if ($askedStatus == 'open') {
      return empty($this->end_time) || empty($this->end_time_hard) ? TRUE : FALSE;
    }
    else {
      return empty($this->end_time) || empty($this->end_time_hard) ? FALSE : TRUE;
    }
  }

  /**
   * Building the shift title.
   */
  function shiftTitle($entity) {
    $entity->title = t('Start: @start', array(
      '@start' => format_date($entity->start_time_hard, 'custom', variable_get('date_format_office_module_format_date')),
    ));

    if ($entity->end_time_hard) {
      $entity->title .= ' ' . t('End: @end', array(
        '@end' => format_date($entity->end_time_hard, 'custom', variable_get('date_format_office_module_format_date')),
      ));
    }
    else {
      $entity->title .= t('(Open shift)');
    }
  }
}

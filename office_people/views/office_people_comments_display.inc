<?php

/**
 * @file
 * Views handler for displaying a the comments.
 */
class office_people_comments_display extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function render($values) {
    $account = office_user_init();
    $comment = (object)array(
      'nid' => $values->nid,
      'uid' => $account->uid,
      'node_type' => 'comment_node_office_customer_message',
      'language' => LANGUAGE_NONE,
    );
    $form = drupal_get_form('comment_node_office_customer_message_form', $comment);
    $output = views_embed_view('comments', 'customer_message_comments', $values->nid);
    $output .= l(t('Add comment'), '#', array(
      'attributes' => array(
        'id' => array(
          'office-people-add-comment',
        ),
      ),
    ));
    $output .= render($form);
    return $output;
  }
}

<div class='top clearfix'>
  <div class='first'>
    <?php print $shift_link; ?>
  </div>
  <div class='second'>
    <?php print $quick_user_form; ?>
  </div>
</div>
<div class='body'>
  <div id='tabs'>
    <?php print $tabs; ?>
    <div id='ui-tabs-1' class='clearfix'><?php print $data_center; ?></div>
    <div id='ui-tabs-2'><?php print $settings; ?></div>
    <div id='ui-tabs-3'><?php print $shifts; ?></div>
  </div>
</div>

<?php

/**
 * @file
 * Views handler for display the total cost of the product - quantity * price
 */

class office_hospitality_product_cost extends views_handler_field_entity {

  function render($value) {
    $wrapper = entity_metadata_wrapper('node', $value->nid);

    return $wrapper->field_price->value() * $wrapper->field_quantity->value();
  }
}

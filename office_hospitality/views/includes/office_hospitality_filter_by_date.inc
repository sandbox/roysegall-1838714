<?php
/**
 * @file
 * Views handler for filter products by purchase date.
 */

/**
 * Writing a custom filter by date handler for two main reason:
 *
 * 1. The start and end date are nesscory for executing the search. I don't
 *    think that the user need to enter both of the fields for filtering
 *    products.
 *
 * 2. A product that was bought in the 29/03/2013 and the user set the start
 *    date and the end date to the 29/03/2013, the product will not be shown
 *    because the date is not between the 29/03/2013.
 */
class office_hospitality_filter_by_date extends views_handler_filter {

  public $date_handler = NULL;

  /**
   * Add setting to the handler form: the field that will be added to the query.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // No multiple selection please.
    $form['expose']['multiple']['#access'] = FALSE;

    // Adding two settings of my own.
    $form['field_name'] = array(
      '#type' => 'select',
      '#title' => t('Field name'),
      '#description' => t('Select the field you would like to add in the search.'),
      '#options' => array('none' => t('-- Select a field --')) + $this->fields_date(),
      '#weight' => 0,
      '#default_value' => $this->options['field_name'],
    );

    $form['switch_when_bigger'] = array(
      '#type' => 'checkbox',
      '#title' => t('Switch between dates inputs when end time is before start time'),
      '#description' => t('If the start date is after the end time(or the opposite), the values will be switched.'),
      '#weight' => 0,
      '#default_value' => $this->options['switch_when_bigger'],
    );
  }

  /**
   * Collect all the date fields type.
   *
   * @return
   *  List of date fields.
   */
  public function fields_date() {
    $node_info = entity_get_info('node');
    $bundles = array_keys($node_info['bundles']);
    $fields = array();
    foreach ($bundles as $bundle) {
      $fields_info = field_info_instances('node', $bundle);
      foreach ($fields_info as $field_name => $field_info) {
        if ($field_info['widget']['module'] != 'date') {
          continue;
        }

        $fields[$field_name] = $field_info['label'];
      }
    }

    return $fields;
  }

  /**
   * Pre defined settings.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['field_name'] = array('default' => '');
    $options['switch_when_bigger'] = array('default' => '');

    return $options;
  }

  public function accept_exposed_input($input) {
  }

  /**
   * Add the selectors to the value form using the date handler.
   */
  public function value_form(&$form, &$form_state) {
    $form['value'] = array();
    $form['value']['#tree'] = TRUE;

    $form['office'] = array(
      '#prefix' => "<div id='office-hospitality-dates-picker' class='clearfix'>",
      '#suffix' => '</div>',
    );

    $form['office']['start_date'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
      '#title' => t('Start date'),
      '#date_label_position' => 'none',
    );
    $form['office']['end_date'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
      '#title' => t('End date'),
      '#date_label_position' => 'none',
    );
  }

  public function exposed_submit(&$form, &$form_state) {
    if (!$this->options['field_name']) {
      drupal_set_message(t('There is no field to run the query against. Please define the field for filter date format.'), 'error');
      return;
    }

    if (empty($this->view->exposed_input['start_date']['date']) && empty($this->view->exposed_input['end_date']['date'])) {
      return;
    }

    // Get the date values in timestamp.
    $start_date = empty($this->view->exposed_input['start_date']['date']) ? '' : strtotime($this->view->exposed_input['start_date']['date']);
    $end_date = empty($this->view->exposed_input['end_date']['date']) ? '' : strtotime($this->view->exposed_input['end_date']['date']);

    if ($start_date > $end_date && $this->options['switch_when_bigger']) {
      $dummy = $start_date;
      $start_date = $end_date;
      $end_date = $dummy;
    }

    // Get the field data: table name and column.
    $field_info = field_info_field($this->options['field_name']);
    $table = key($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT']);
    $column = $field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table][key($field_info['columns'])];

    // Adding relation ship for the table of the field.
    $this->query->add_table($table, $this->relationship);

    // Alter the query.
    if (!empty($start_date) && empty($end_date)) {
      $this->query->add_where($this->options['group'], "{$table}.{$column}", $start_date, '>=');
    }
    else if (empty($start_date) && !empty($end_date)) {
      $this->query->add_where($this->options['group'], "{$table}.{$column}", $end_date, '<=');
    }
    else {
      $this->query->add_where_expression($this->options['group'], "{$table}.{$column} >= {$start_date} AND {$table}.{$column} <= {$end_date}");
    }
  }
}

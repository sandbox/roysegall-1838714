<?php
/**
 * @file
 * Main class of the shift handler that will contain functions.
 *
 * Contain some api function for the handlers, variables etc. etc.
 */

class shiftHandler {
  /**
   * The account of the user. Initialize in the init function.
   */
  public $account;

  /**
   * The wrapper of the node.
   */
  public $wrapper;

  /**
   * The title of the handler.
   */
  public $title;

   /**
    * The description of the handler.
    */
  public $description;

  /**
   * Contains the settings of the handler.
   */
  public $settings;

  /**
   * Contain the Profit of the action, such as: calculating the wage for the
   * shift/month.
   */
  public $Profit = 0;

  /**
   * The fields that the classes need to work with.
   */
  public $fields;

  /**
   * Debug format date for unix time stamp.
   */
  public $formatDate = 'H:i d/m/y';

  /**
   * The function for debug.
   */
  public $debugFunction = 'dpm';

  /**
   * Construct the wrapper object to the node of the shift.
   *
   * @param $node
   *  The node of the shift. Will be call during constructing automatically.
   *
   * @return shiftHandler
   *  The called object.
   */
  function wrapper($shift) {
    $this->wrapper = entity_metadata_wrapper('shift', $shift);
    $this->account = $this->wrapper->owner;

    return $this;
  }

   /**
    * Define fields to work with from the entity.
    *
    * @param $key
    *   The key inside the array of the fileds. Holds the wrapper property value.
    * @param $wrapperProperty
    *   The wrapper property.
    *
    * @return shiftHandler
    *  The called object.
    */
  function fields($key, $wrapperProperty) {
    if (!isset($this->wrapper->{$wrapperProperty})) {
      drupal_set_message(t("The field @field don't exists in the content type @bundle.",
        array(
          '@field' => $wrapperProperty,
          '@bundle' => $this->wrapper->getBundle(),
        )
      ), 'error');

      $this->fields[$key] = NULL;
      return $this;
    }

    $this->fields[$key] = $this->wrapper->{$wrapperProperty}->value();
    return $this;
  }

  /**
   * Adding settings for the handler.
   *
   * The handler settings are saved as system variables. This function load them
   * into the $this->settings variable.
   *
   * @return shiftHandler
   *  The called object.
   */
  function  addSettings($settings) {
    // Init it as an object.
    $this->settings = new stdClass;

    foreach ($settings as $key) {
      $this->settings->{$key} = variable_get($key);
    }

    return $this;
  }

  /**
   * You can also print the Profit with this function.
   */
  function __toString() {
    return (string)$this->Profit;
  }

  /**
   * Debug me!
   *
   * @return shiftHandler
   *  The called object.
   */
  function debug() {
    $debug = clone $this;

    foreach (get_object_vars($debug) as $key => $value) {
      if ($this->{$key} instanceof EntityMetadataWrapper) {
        $debug->{$key} = $debug->{$key}->value();
      }
    }

    call_user_func($this->debugFunction, $debug);

    return $this;
  }

  /**
   * Change debug function.
   *
   * @param $function
   *  The function for replace the debug function.
   *
   * @return shiftHandler
   *  The called object.
   */
  function changeDebugFunction($function) {
    $this->debugFunction = $function;

    return $this;
  }

  /**
   * Debug the a time stamp.
   *
   * @param $time
   *  The time stamp.
   *
   * @return shiftHandler
   *  The called object.
   */
  function debugTimestamp($time) {
    call_user_func($this->debugFunction, date($this->formatDate, $time));

    return $this;
  }

  /**
   * Change format datefor debugging.
   *
   * @param $format
   *  The format for replace the format of the timestamp debug.
   *
   * @return shiftHandler
   *  The called object.
   */
  function changeDateFormat($format) {
    $this->formatDate = $format;

    return $this;
  }

  /**
   * Executing.
   */
  function execute() {
    return $this;
  }
}

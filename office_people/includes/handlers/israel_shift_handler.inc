<?php
/**
 * @file
 * Israel shift handler. Dealing with the labor law in israel.
 */
class office_people_shift_handler_israel extends shiftHandler {

  /**
   * Contructing the class - include a title and description by default.
   */
  function __construct() {
    $this->addSettings(array_keys($this->handler_settings()));

    $this->title = t('Shift handler for israel');
    $this->description = t('Calculate the wage of the shift\'s acording to israel labor law');
  }

  /**
   * Executing.
   *
   * In israel we can't have a shift that is long then 8 hours. If the shift was
   * more then 8 hours then for the 9th and the 10th we get 125 precent, and
   * for the 11th and beyong we get 150 precent.
   *
   * If the employee worked in shabat then he need to get 200 precent.
   */
  function execute() {
    // Add fields.
    $this
      ->fields('start','start_time')
      ->fields('end','end_time');

    // Start calculate.
    $seconds = $this->fields['end'] - $this->fields['start'];
    if ($seconds < 0) {
      $seconds = 0;
    }

    $wage = $this->account->field_wage_per_hour->value();
    $hours = $seconds / 3600;

    // Check first if the employee worked on friday/staurday.
    $start_day = getdate($this->fields['start']);
    $shabat = array();

    if (in_array($start_day['wday'], array(5, 6))) {
      $sun = date_sun_info($this->fields['start'], $this->settings->shift_handler_latitude, $this->settings->shift_handler_longitude);

      // The shabat enter 18 minutes before the sunset.
      $shabat['entrance'] = $sun['sunset'] - (18 * 60);

      // Shabat exit 25 hours after she entered.
      $shabat['exit'] = $shabat['entrance'] + (60*60);
      $exitInfo = getdate($shabat['entrance']);

      // The start day was in friday so we need to add 24 hours because we added
      // 1 hour before.
      if ($exitInfo['wday'] != 6) {
        $shabat['exit'] = $shabat['exit'] + (60*60*24);
      }

      // Worked in shabat. Need to calculate the seconds he worked in as 200%
      // and the hours before/after the shabat need to calculate as regular
      // rules.
      if ($this->fields['end'] < $shabat['exit'] || $this->fields['start'] > $shabat['entrance']) {
        $regular_hours = 0;
        if (($before_shabat = $shabat['entrance'] - $this->fields['start']) > 0) {
          $regular_hours = $before_shabat / 3600;
        }

        if (($after_shabat = $this->fields['end'] - $shabat['exit']) > 0) {
           $regular_hours = $after_shabat / 3600;
        }

        if ($regular_hours > $hours) {
          $speical_hours =  $regular_hours - $hours;
        }
        else {
          $speical_hours = $hours - $regular_hours;
        }

        $this->Profit = $wage * 2 * $speical_hours;
        $this->calculateNormalHours($regular_hours);
        $this->Profit = round($this->Profit);

        return $this;
      }
    }

    $this->calculateNormalHours($hours);

    return $this;
  }

  /**
   * Calculate the hours for noral days.
   */
  function calculateNormalHours($hours) {
    $wage = $this->account->field_wage_per_hour->value();

    if ($hours <= 8) {
      $this->Profit += $hours * $wage;
    }
    else {
      $this->Profit += 8 * $wage;
      $overtime_hours = $hours - 8;

      if ($overtime_hours <= 2) {
        $this->Profit += $overtime_hours * 1.25 * $wage;
      }
      else {
        // Get the 125 precent.
        $this->Profit += 2 * 1.25 * $wage;

        // Get the 150 precent.
        $this->Profit += ($overtime_hours - 2) * 1.50 * $wage;
      }
    }

    return $this;
  }

  /**
   * Return settings about the handler.
   */
  function handler_settings() {
    $form['shift_handler_latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#description' => t('The latitude of the city - for calculating shabat times.'),
    );

    $form['shift_handler_longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#description' => t('The longitude of the city - for calculating shabat times.'),
    );

    $form['shift_handler_coin'] = array(
      '#type' => 'textfield',
      '#title' => t('Coin symbol'),
      '#description' => t('The coin symbol to be attached near the Profit value.'),
    );

    return $form;
  }

  function __toString() {
    return (string)$this->Profit . $this->settings->shift_handler_coin;
  }
}

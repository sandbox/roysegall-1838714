<?php

/**
 * @file
 * Token intergration for office people module.
 */

/**
 * Implements hook_token_info().
 */
function office_people_token_info() {
  $info['tokens']['node']['customer-message-direct-link'] = array(
    'name' => t('Customer message direct link'),
    'description' => ('Generate a link for the customer message.'),
  );

  $info['tokens']['comment']['customer-message-direct-link'] = array(
    'name' => t('Customer comment direct link'),
    'description' => ('Generate a link for the customer message that the comment belong to.'),
  );

  $info['tokens']['message']['message-shift-data-table'] = array(
    'name' => t('Shifts information'),
    'description' => ('Generate table with the shifts the message is referenced two.'),
  );

  $info['tokens']['shift']['start-time'] = array(
    'name' => t('Shift data start time'),
    'description' => ('Present the start time of the shift.'),
  );

  $info['tokens']['shift']['end-time'] = array(
    'name' => t('Shift data end time'),
    'description' => ('Present the end time of the shift.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function office_people_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Only nodes.
  if (!in_array($type, array('node', 'comment', 'message', 'shift'))) {
    return;
  }

  foreach ($tokens as $name => $original) {
    // Handling customer message direct link.
    switch ($name) {
      case 'customer-message-direct-link':
        if ($type == 'node') {
          $nid = $data['node']->nid;
          $wrapper = entity_metadata_wrapper('node', $nid);
        }
        else {
          $wrapper = entity_metadata_wrapper('comment', $data['comment']);
          $nid = $wrapper->node->getIdentifier();
        }

        // Get the message relate to the entity the token relate to.
        $query = new entityFieldQuery();
        $result = $query
          ->entityCondition('entity_type', 'message')
          ->propertyCondition('type', $type == 'node' ? 'new_customer_message' : 'new_customer_comment')
          ->fieldCondition('field_' . $type . '_ref', 'target_id', $wrapper->getIdentifier())
          ->range(0, 1)
          ->execute();

        // No messages. Return.
        if (empty($result['message'])) {
          $replacements[$original] = '';
          break;
        }

        // Wrap up the entity.
        $mid = array_keys($result['message']);
        $entity_wrapper = entity_metadata_wrapper('message', $mid[0]);
        $menu = menu_get_item('admin/structure/office/people');

        // Check if the user has access to the admin office page.
        $access = TRUE;
        foreach (unserialize($menu['access_arguments']) as $permission) {
          if (!user_access($permission, $entity_wrapper->user->value())) {
            $access = FALSE;
            break;
          }
        }

        // Generate the correct path.
        if ($access) {
          $path = 'admin/structure/office/people/customer-message/' . $nid;
        }
        else {
          $path = 'user/' . $entity_wrapper->user->getIdentifier() . '/office_people/customer-message/' . $nid;
        }

        $replacements[$original] = l(t('Go to message'), $path, array(
          'absolute' => TRUE,
        ));
        break;

      case 'message-shift-data-table':
        $wrapper = entity_metadata_wrapper('message', $data['message']);
        $nids = office_extract_ids($wrapper, "field_shifts");
        $replacements[$original] = office_people_shifts_list_table($nids, TRUE);
        break;

      case 'start-time':
      case 'end-time':
        $field = str_replace('-', "_", $name);
        $replacements[$original] = format_date($data['shift']->{$field}, 'custom', 'H:i');
        break;
    }
  }

  return $replacements;
}

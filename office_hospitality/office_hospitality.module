<?php

/**
 * @file
 * Module functionallty file.
 */

/**
 * Implements hook_menu().
 */
function office_hospitality_menu() {
  $items = array();

  $items['admin/structure/office/hospitality'] = array(
    'title' => 'Hospitality',
    'description' => 'Manage your office items',
    'access arguments' => array('office hospitality general access'),
    'page callback' => 'office_hospitality_main_page',
  );

  $items['admin/structure/office/hospitality/main'] = array(
    'title' => 'Hospitality',
    'description' => 'Manage your office items',
    'access arguments' => array('office hospitality general access'),
    'page callback' => 'office_hospitality_main_page',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/structure/office/hospitality/settings'] = array(
    'title' => 'Settings',
    'description' => 'Settings of the hospitality.',
    'access arguments' => array('office hospitality settings access'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('office_hospitality_settings'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['office_hospitality/autocomplete'] = array(
    'title' => 'Office hospitality user auto complete',
    'page callback' => 'office_hospitality_auto_complete',
    'access arguments' => array('office hospitality general access'),
    'type' => MENU_LOCAL_ACTION,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function office_hospitality_permission() {
  return array(
    'office hospitality general access' => array(
      'title' => t('General access'),
      'description' => t('Grant a general access to the office hospitality admin page.'),
    ),
    'office hospitality settings access' => array(
      'title' => t('Settings access'),
      'description' => t('Allow the user to manage the hospitality settings.'),
    ),
  );
}

/**
 * Implements hook_node_info().
 */
function office_hospitality_node_info() {
  $items = array(
    'hospitality_office_products' => array(
      'name' => t('Hospitality office products'),
      'base' => 'node_content',
      'description' => t('Products you bought for your office such as coffee, milk, sugar etc. etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Menu callback - hospitality main page.
 */
function office_hospitality_main_page() {
  $nids = office_hospitality_get_items_id();
  if (!empty($nids)) {
   $nids = implode('+', $nids);
  }
  else {
    return t("There are no office products. Please go and <a href='@url'>add more products</a>", array(
      '@url' => url('node/add/hospitality-office-products'),
    ));
  }

  $output = views_embed_view('office_hospitality_items', 'hospitality_items', $nids);

  return $output;
}

/**
 * Return the items that the user can see or manage.
 *
 *  @param $user
 *    The user object or id.
 *
 * @return The items the user can manage.
 */
function office_hospitality_get_items_id($user = NULL) {
  $account = office_user_init($user);

  $query = office_field_query_init()
    ->propertyCondition('type', 'hospitality_office_products');

  if (!in_array('administrator', $account->roles) && !in_array('office manager', $account->roles)) {
    $query->propertyCondition('uid', $account->uid);
  }

  $result = $query->execute();

  return !empty($result['node']) ? array_keys($result['node']) : FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function office_hospitality_form_views_form_office_hospitality_items_hospitality_items_alter(&$form, &$form_state) {
  if ($form_state['build_info']['args']['0']->name != 'office_hospitality_items') {
    return;
  }

  $form['#after_build'][] = 'office_hospitality_VBO_action_after_build';

  // Don't need to add css for VBO submit form.
  if (isset($form_state['clicked_button'])) {
    if ($form_state['operation'] instanceof ViewsBulkOperationsAction && $form_state['operation']->operationId == 'action::node_assign_owner_action') {
      // The core functionality for changing the content author need to be
      // different because it's not change the user uid by the user field.
      foreach($form_state['editablefields_entities']['node'] as $node) {
        if ($node->type != 'hospitality_office_products') {
          return;
        }
      }

      // Remove the old select list.
      $form['owner_name'] = array();

      // Auto complete input text.
      $form['author']['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Authored by'),
        '#maxlength' => 60,
        '#autocomplete_path' => 'office_hospitality/autocomplete',
        '#default_value' => $node->name,
        '#weight' => -1,
      );

      // Custom validate function.
      $form['actions']['submit']['#validate'] = array('office_hospitality_vbo_change_autor_validate');
      // Custom submit function.
      $form['actions']['submit']['#submit'] = array('office_hospitality_vbo_change_author_submit');

    }
    return;
  }

  $form['#attached']['css'] = array(drupal_get_path('module', 'office_hospitality') . '/css/office_hospitality.css');
  $form['#attached']['js'] = array(drupal_get_path('module', 'office_hospitality') . '/js/office_hospitality.js');

  if (!empty($form['title_field'])) {
    foreach (array_keys($form['title_field']) as $key) {
      $form['title_field'][$key]['title_field'][LANGUAGE_NONE][0]['#description'] = '';
    }
  }
}

/**
 * Make sure that the user checked products.
 */
function office_hospitality_VBO_action_after_build(&$form, $form_state) {
  $form['select']['operation']['#options']['action::node_assign_owner_action'] = t('Change owner of the products.');

  // Displaying only published. It doesn't seem right to display an option to
  // public them.
  unset($form['select']['operation']['#options']['action::node_publish_action']);

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function office_hospitality_form_hospitality_office_products_node_form_alter(&$form, &$form_state) {
  // Adding redirect handler.
  $form['actions']['submit']['#submit'][] = 'office_hospitality_redirect_after_add_product';

  // When adding a product ask the user if he want to decrease the value of the
  // piggy bank.
  $form['piggy_bank'] = array(
    '#type' => 'fieldset',
    '#title' => t('Piggy bank'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
  );

  $form['piggy_bank']['actions'] = array(
    '#type' => 'select',
    '#title' => t('Select an action'),
    '#description' => t('How would you like to act?'),
    '#options' => array(
      'decrease_product_price' => t('Decrease the value of this entry from the piggy bank total value.'),
      'set_another_value' => t('Change the piggy bank value to another.'),
    ),
  );

  $form['piggy_bank']['piggy_bank_new_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the new value of the piggy bank'),
    '#states' => array(
      'visible' => array(
        ':input[name="actions"]' => array('value' => 'set_another_value'),
      ),
    ),
  );

  $form['#validate'][] = 'office_hospitality_validate_piggy_bank_action';

  // Default value for the user field for the user.
  if (!empty($form['field_product_owner'][LANGUAGE_NONE][0]['target_id']['#default_value'])) {
    return;
  }

  global $user;
  $account = user_load($user->uid);
  $form['field_product_owner'][LANGUAGE_NONE][0]['target_id']['#default_value'] = "{$account->name} ({$account->uid})";
}

/**
 * Verify the user has entered a value for the new piggy bank value.
 */
function office_hospitality_validate_piggy_bank_action($form, $form_state) {
  if ($form_state['values']['actions'] == 'set_another_value' && $form_state['values']['piggy_bank_new_value'] == NULL) {
    form_set_error('piggy_bank_new_value', t('Enter the new value of the piggy bank.'));
  }
}

/**
 * After adding a product - set a new value for the piggy bank and redirect the
 * user.
 */
function office_hospitality_redirect_after_add_product($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/office/hospitality';

  if ($form_state['values']['actions'] == 'set_another_value') {
    // Set the new value of the piggy bank.
    variable_set('piggy_bank', $form_state['values']['piggy_bank_new_value']);
  }
  else {
    // Decrease the value of the entry from the piggy bank.
    $piggy_bank = variable_get('piggy_bank');
    $wrapper = entity_metadata_wrapper('node', $form_state['values']['nid']);
    $piggy_bank = $piggy_bank - ($wrapper->field_price->value() * $wrapper->field_quantity->value());
    variable_set('piggy_bank', $piggy_bank);
  }
}

/**
 * Custom validate handler - check that the assigned user name is exists.
 */
function office_hospitality_vbo_change_autor_validate($form, &$form_state) {
  $query = new EntityFieldQuery();
  $result = $query
    ->entityCondition('entity_type', 'user')
    ->propertyCondition('name', $form_state['values']['name'])
    ->execute();

  if (empty($result['user'])) {
    form_set_error("name", t("A user with that name wasn'nt found"));
  }

  $uid = reset(array_keys($result['user']));
  $form_state['uid'] = $uid;
}

/**
 * Custom submit handler - changing the author of the nodes.
 */
function office_hospitality_vbo_change_author_submit($form, $form_state) {
  foreach($form_state['editablefields_entities']['node'] as $node) {
    if ($node->type != 'hospitality_office_products') {
      continue;
    }
    $wrapper = entity_metadata_wrapper('node', $node);
    $wrapper->author->set($form_state['uid']);
    $wrapper->field_product_owner->set($form_state['uid']);
    $wrapper->save();
  }
}

/**
 * Implements hook_node_update().
 *
 * The module editablefields cannot add the author of the node as a editable
 * formatter because this is a property and not a field. Using a field that
 * holds a reference to the user, we can manage the field and the owner of the
 * node together and still allow to edit the node author via AJAX for UX.
 */
function office_hospitality_node_presave($node) {
  if ($node->type != 'hospitality_office_products') {
    return;
  }

  $wrapper = entity_metadata_wrapper('node', $node);
  $node->uid = $wrapper->field_product_owner->getIdentifier();
}

/**
 * Auto complete path.
 */
function office_hospitality_auto_complete($string = '') {
  $matches = array();
  if ($string) {

    $query = new entityFieldQuery();
    $result = $query
      ->entityCondition('entity_type', 'user')
      ->propertyCondition('name', $string, 'CONTAINS')
      ->range(0, 10)
      ->execute();

    if (!empty($result['user'])) {
      $users = user_load_multiple(array_keys($result['user']));
      foreach ($users as $user) {
        $matches[$user->name] = check_plain($user->name);
      }
    }
  }

  drupal_json_output($matches);
}

/**
 *  Implements hook_views_pre_render().
 */
function office_hospitality_views_pre_render(&$view) {
  if ($view->name != 'office_hospitality_items') {
    return;
  }

  $info = office_hospitality_piggy_bank_info(variable_get('piggy_bank_method'));

  if (!function_exists($info['callback'])) {
    drupal_set_message(t('The @function was not found', array('@function' => $info['callback'])), 'error');
  }
  else {
    if (!empty($info['arguments'])) {
      $output = call_user_func_array($info['callback'], $info['arguments']);
    }
    else {
      $output = call_user_func($info['callback']);
    }

    if (is_array($output)) {
      $view->attachment_before = render($output);
    }
  }

  drupal_add_library('system', 'ui.dialog');
  $view->attachment_after .= "<div id='dialog' title='" . t('Fill the form correctly') . "'>
    <p>" . t("Please fill the form correctly: choose an operation and products.") . "</p></div>";
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function office_hospitality_form_views_exposed_form_alter(&$form, $form_state) {
  if ($form['#id'] != 'views-exposed-form-office-hospitality-items-hospitality-items') {
    return;
  }
  $form['#after_build'][] = 'office_hospitality_remove_date_description';
}

/**
 * Custom callback function - remove the views filter description.
 */
function office_hospitality_remove_date_description($form, $form_state) {
  $form['field_purchase_date_value']['min']['date']['#description'] = '';
  $form['field_purchase_date_value']['max']['date']['#description'] = '';
  return $form;
}

/**
 * Implements hook_views_api().
 */
function office_hospitality_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'office_hospitality') . '/views',
  );
}

/**
 * Implements hook_preprocess_views_view_table().
 */
function office_hospitality_preprocess_views_view_table(&$variables) {
  if ($variables['view']->name != 'office_hospitality_items') {
    return;
  }
  // Building the text.
  $sum = 0;
  foreach ($variables['rows'] as $key) {
    $sum += $key['office_hospitality_product_cost'];
  }

  $params = array(
    '@total' => $sum,
  );

  $variables['rows'][] = array('views_bulk_operations' => t('Total: @total', $params));

  // Adding a class and field classes.
  $variables['field_attributes']['views_bulk_operations'][] = array('colspan' => 8);
  $variables['field_classes']['views_bulk_operations'][] = array();

  // Minor css games: apply the "views-row-last" class to the new td and remove
  // from the old and build the odd/even class.
  $row_keys = array_keys($variables['row_classes']);
  $last_key = end($row_keys);
  $row_last_key = array_search('views-row-last', $variables['row_classes'][$last_key]);

  $class = $variables['row_classes'][$last_key][0] == 'even' ? 'odd' : 'even';
  $variables['row_classes'][$last_key][$row_last_key] = '';
  $variables['row_classes'][] = array('views-row-last', $class, 'total-amount');
}

/**
 * Invoking data from other modules about the current piggy bank money info.
 *
 * Other modules can implement a different way to calculate the piggy bank cache
 * info instead of a variable_set()/variable_get().
 */
function office_hospitality_piggy_bank_info($method = NULL) {
  $piggy_bank_info = &drupal_static(__FUNCTION__);

  if (!isset($piggy_bank_info)) {
    if ($cache = cache_get('office_hospitality_piggy_bank_info')) {
      $piggy_bank_info = $cache->data;
    }
    else {
      // Get all the data from other hooks.
      $piggy_bank_info = module_invoke_all('office_hospitality_piggy_bank_info');

      // Alter the information.
      drupal_alter('office_hospitality_piggy_bank_info', $piggy_bank_info);

      // Save piggy bank info.
      cache_set('office_hospitality_piggy_bank_info', $piggy_bank_info, 'cache');
    }
  }

  return empty($method) ? $piggy_bank_info : $piggy_bank_info[$method];
}

/**
 * Implements hook_office_hospitality_piggy_bank_info().
 */
function office_hospitality_office_hospitality_piggy_bank_info() {
  return array(
    'office_hospitality_piggy_bank' => array(
      'title' => t('Office hospitality'),
      'callback' => 'drupal_get_form',
      'arguments' => array('office_hospitality_piggy_bank_form'),
    ),
    'office_hospitality_piggy_bank_nojs' => array(
      'title' => t('Office hospitality (without AJAX)'),
      'callback' => 'drupal_get_form',
      'arguments' => array('office_hospitality_piggy_bank_form_nojs'),
    ),
  );
}

/**
 * Settings forms.
 */
function office_hospitality_settings($form, $form_state) {
  $piggy_bank = office_hospitality_piggy_bank_info();

  $options = array();
  foreach ($piggy_bank as $method => $info) {
    $options[$method] = $info['title'];
  }

  $form['piggy_bank_method'] = array(
    '#type' => 'select',
    '#title' => t('Piggy bank method'),
    '#description' => t('Select the method that the piggy bag will be manage.'),
    '#options' => array('---' => t('Select a method')) + $options,
    '#default_value' => variable_get('piggy_bank_method', ''),
  );

  return system_settings_form($form);
}

/**
 * Validate function - verify the user select a valid option.
 */
function office_hospitality_settings_validate($form, $form_state) {
  if ($form_state['values']['piggy_bank_method'] == '---') {
    form_set_error('piggy_bank_method', t('Please select method.'));
  }
}

/**
 * Form of the office hospitality piggy bank callback.
 */
function office_hospitality_piggy_bank_form($form, $form_state) {
  $form['piggy_bank'] = array(
    '#type' => 'textfield',
    '#title' => t('Piggy bank'),
    '#size' => 10,
    '#default_value' => variable_get('piggy_bank'),

    '#ajax' => array(
      'callback' => 'office_hospitality_piggy_bank_form',
      'replace' => 'replace',
    ),
  );

  $piggy_bank = !empty($form_state['values']['piggy_bank']) ? $form_state['values']['piggy_bank'] : FALSE;

  if ($piggy_bank) {
    variable_set('piggy_bank', $piggy_bank);
    $form['piggy_bank']['#default_value'] = $piggy_bank;
  }

  return $form;
}

/**
 * Ajax callback - return part of the form.
 */
function office_hospitality_piggy_bank_form_ajax($form, $form_state) {
  return $form['piggy_bank'];
}

/**
 * Piggy bank form callback - update the piggy bank without AJAX callback.
 */
function office_hospitality_piggy_bank_form_nojs($form, $form_state) {
  $form['piggy_bank'] = array(
    '#type' => 'textfield',
    '#title' => t('Piggy bank'),
    '#size' => 10,
    '#required' => TRUE,
    '#default_value' => variable_get('piggy_bank'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    ),
  );

  return $form;
}

/**
 * Submit handler for the non AJAX piggy bank form.
 */
function office_hospitality_piggy_bank_form_nojs_submit($form, $form_state) {
  variable_set('piggy_bank', $form_state['values']['piggy_bank']);
}

/**
 * Implement hook_fields_info().
 */
function office_hospitality_office_fields_info() {
  $return = array();

  $return['title_field'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'title_field',
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => '1',
      'type' => 'text',
    ),
    'instance' => array(
      'description' => 'A field replacing node title.',
      'label' => 'Title',
      'required' => TRUE,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => '0',
      ),
    ),
  );

  $return['hospitality-field_product_owner'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'field_product_owner',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'target_bundles' => array(),
        ),
        'target_type' => 'user',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'instance' => array(
      'label' => 'User',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'entityreference',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'path' => '',
          'size' => '60',
        ),
        'type' => 'entityreference_autocomplete',
        'weight' => '6',
      ),
    ),
  );

  $return['field_quantity'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'field_quantity',
      'module' => 'number',
      'type' => 'number_integer',
    ),
    'instance' => array(
      'label' => 'Quantity',
      'required' => 1,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '4',
      ),
    ),
  );

  $return['field_purchase_date'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'field_purchase_date',
      'module' => 'date',
      'settings' => array(
        'cache_count' => '4',
        'cache_enabled' => 0,
        'granularity' => array(
          'day' => 'day',
          'hour' => 0,
          'minute' => 0,
          'month' => 'month',
          'second' => 0,
          'year' => 'year',
        ),
        'timezone_db' => '',
        'todate' => '',
        'tz_handling' => 'none',
      ),
      'translatable' => '0',
      'type' => 'datestamp',
    ),
    'instance' => array(
      'label' => 'Purchase date',
      'required' => 1,
      'settings' => array(
        'default_value' => 'now',
        'default_value2' => 'same',
        'default_value_code' => '',
        'default_value_code2' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'date',
        'settings' => array(
          'increment' => '15',
          'input_format' => 'd/m/Y - H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_popup',
        'weight' => '5',
      ),
    ),
  );

  $return['field_price'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'field_price',
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
      ),
      'translatable' => '0',
      'type' => 'number_float',
    ),
    'instance' => array(
      'label' => 'Price',
      'required' => 1,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '2',
      ),
    ),
  );

  $return['field_description'] = array(
    'config' => array(
      'cardinality' => '1',
      'field_name' => 'field_description',
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'instance' => array(
      'label' => 'Description',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '1',
      ),
    ),
  );

  return $return;
}

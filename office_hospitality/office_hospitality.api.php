<?php

/**
 * @file
 * Office hospitality integration documentation file.
 */

/**
 * Implements hook_office_hospitality_piggy_bank_info().
 */
function hook_office_hospitality_piggy_bank_info() {
  return array(
    'office_hospitality_piggy_bank' => array(
      'title' => t('Office hospitality'),
      'callback' => 'drupal_get_form',
      'arguments' => array('office_hospitality_piggy_bank_form'),
    ),
  );
}

/**
 * Implements hook_office_hospitality_piggy_bank_info().
 */
function hook_office_hospitality_piggy_bank_info_alter(&$data) {
  $data['office_hospitality_piggy_bank']['callback'] = 'foo';
}

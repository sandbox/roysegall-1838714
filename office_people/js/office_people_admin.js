/**
 * @file
 * Office people main admin page JS handling.
 */
(function ($) {

  /**
   * The quick search users AJAX handeling area.
   */
  Drupal.behaviors.quickUsersSearch = {
    attach: function() {
      $("#edit-username").keyup(function() {
        var text = $(this).val();

        var address = '?q=admin/structure/office/people/json-list/' +  text;
        if (text.length == 0) {
          $("#office_people_user_results").hide().html('');
        }
        else {
          $.getJSON(address, function(data) {
            var items = [];
            $.each(data, function(key, val) {
              items.push("<div class='quick-user-result'>" + val.name + "<br /> " +
              "<span>" + val.shift_option + "</span></div>");
            });

            $("#office_people_user_results").show().html(items.join(''));
          });
        }
      });
    }
  }

  /**
   * Apply jquery UI tabs option.
   */
  Drupal.behaviors.tabs = {
    attach: function() {
      $("#tabs").tabs();
    }
  }

  /**
   * Form validation.
   */
  Drupal.behaviors.FormValidation = {
    attach: function() {
      $("#edit-submit").click(function () {
        var ok = true;
        if ($("#office-customer-message-node-form #edit-title").val() == '') {
          $("#office-customer-message-node-form #edit-title").css('border', 'red solid 2px');
          ok = false;
        }
        else {
          $("#office-customer-message-node-form #edit-title").css();
        }

        if ($("#office-customer-message-node-form #edit-field-user-und-0-target-id").val() == '') {
          $("#office-customer-message-node-form #edit-field-user-und-0-target-id").css('border', 'red solid 2px');
          ok = false;
        }

        if (!ok) {
          event.preventDefault();
        }
      });
    }
  }

  /**
   * Show and hide the comment for the messages form.
   */
  Drupal.behaviors.ShowCommentUI = {
    attach: function() {
      $(".views-field.views-field-comment-display a").toggle(function() {
        event.preventDefault();
        $(this).parent().find('.comment-form').show();
      },
      function() {
        $(this).parent().find('.comment-form').hide();
      });
    }
  }

  /**
   * Select tabs by the js settings.
   */
  Drupal.behaviors.selectTabs = {
    attach: function(context, settings) {
      if (settings.office_people == undefined) {
        return;
      }
      if (settings.office_people.tab == 'options') {
        $(context).find("a[href='#ui-tabs-3']").click();
      }
      else if (settings.office_people.tab == 'shifts') {
        $(context).find(".ui-tabs-nav li:nth-child(2) a").click();
      }
    }
  }
})(jQuery);
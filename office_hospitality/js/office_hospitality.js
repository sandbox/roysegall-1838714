/**
 * @file
 * Javascript file for the office hospitality.
 */

(function ($) {
  Drupal.behaviors.officeHpitalityJavaScript = {
    attach: function(context, settings) {
      // First hide the input the element.
      $("input.editablefield-edit.form-submit.ajax-processed").hide(0, function () {
        // Check if that element has alredy a lazy edit link. If not, add him.
        var element = $(this).parent().find('.lazyload-edit-me');
        if (element.length == 0) {
          $(this).after("<a href='#' class='lazyload-edit-me'>" + Drupal.t('Edit') + "</a>");
        }
      });

      // Only when hovering over the td we can show the edit link.
      $(".views-field-editablefield").hover(function() {
        $(this).find(".lazyload-edit-me").show('fast');
      },
      function () {
        $(this).find(".lazyload-edit-me").hide('fast');
      });

      // Allowing the user to load the input of the field.
      var editClicked = false;
      $(".editablefield-item > .lazyload-edit-me").click(function() {
        $(this).parent().find(".form-submit.ajax-processed").click();
      });

      // Submit the form only when the user checked products.
      $("#views-form-office-hospitality-items-hospitality-items #edit-select").click(function(action) {
        var object = $("#views-form-office-hospitality-items-hospitality-items table.views-table tbody tr");

        var submit = false;
        object.each(function() {
          if ($(this).find(".vbo-select").is(':checked')) {
            submit = true;
          }
        });

        if (!submit || $("#edit-operation").val() == 0) {
          $("#dialog").dialog({
            autoOpen: false,
            minWidth: 500,
            minHeight: 100,
            draggable: false,
            closeOnEscape: true,

            show: {
              effect: "show",
              duration: 500
            },
            hide: {
              effect: "hide",
              duration: 500
            }
          });

          $("#dialog").dialog("open");

          action.preventDefault();
        }
      });
    }
  }
})(jQuery);

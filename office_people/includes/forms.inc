<?php

/**
 * @file
 * Contain the function for the UI of the forms.
 */


/**
 * Implement hook_form_FORM_ID_alter().
 */
function office_people_form_office_customer_message_node_form_alter(&$form, &$form_state) {
  $account = office_user_init();
  $menu = menu_get_item();

  if ($menu['path'] != 'node/%/edit') {
    $form['actions']['preview']['#access'] = FALSE;
  }

  $form['field_user']['#weight'] = -4;
  $form['actions']['submit']['#submit'][] = 'office_people_form_office_customer_message_node_form_submit';
  $form['options']['status']['#default_value'] = TRUE;
  $form['field_user'][LANGUAGE_NONE][0]['target_id']['#required'] = TRUE;

  if (office_role_access(array('office customer'), $account)) {
    $form['field_user']['#access'] = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function office_people_form_comment_node_office_customer_message_form_alter(&$form, &$form_state) {
  $account = office_user_init();

  $form['actions']['preview']['#access'] = FALSE;
  $form['actions']['submit']['#value'] = t('Send');
  $form['subject']['#access'] = FALSE;
  $form['author']['#access'] = FALSE;
  $form['comment_body']['und']['#after_build'][] = 'office_people_comment_hide_text_format';

  $menu = menu_get_item('admin/structure/office/people');
  // Check if the user has access to the admin office page.
  $access = TRUE;
  foreach (unserialize($menu['access_arguments']) as $permission) {
    if (!user_access($permission, $account)) {
      $access = FALSE;
      break;
    }
  }

  // Generate the correct path.
  if ($access) {
    $form_state['redirect_path'] = 'admin/structure/office/people/customer-message/' . $form['#node']->nid;
  }
  else {
    $form_state['redirect_path'] = 'user/' . $account->uid . '/office_people/customer-message/' . $form['#node']->nid;
  }
  $form['#submit'][] = '_office_people_form_comment_node_office_customer_message_form';
}

/**
 * Submit handler; redirect to the spical page for viewing the message.
 */
function _office_people_form_comment_node_office_customer_message_form(&$form, &$form_state) {
  $form_state['redirect'] = $form_state['redirect_path'];
}

/**
 * Custom callback function for remove the text format.
 */
function office_people_comment_hide_text_format($form, &$form_state) {
  hide($form[0]['format']);
  return $form;
}

/**
 * Quick users search.
 */
function office_people_quick_users_search() {
  $form = array();
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Quick search: '),
    '#description' => t('Search users for a quick actions'),
    '#size' => 15,
    '#suffix' => "<div id='office_people_user_results' class='office_people_user_results'></div>",
  );

  return $form;
}

/**
 * Shift entity settings.
 */
function office_people_manage_shift_settings($form, $form_state) {
  $form['shifts'] = array(
    '#title' => t('Shifts'),
    '#description' => t('Enable the option for employees to manage shifts'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('shifts', FALSE),
  );

  $form['shifts_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of shifts'),
    '#description' => t('Number of shifts to display per page'),
    '#default_value' => variable_get('shifts_per_page', 25),
    '#states' => array(
      'visible' => array(
        ':input[name="shifts"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['shiftable_rules'] = array(
    '#title' => t('Allowed roles'),
    '#descripion' => t('Which roles can use the shift manage option.'),
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => user_roles(),
    '#default_value' => array_keys(office_people_shiftable_rules()),
    '#size' => 5,
    '#states' => array(
      'visible' => array(
        ':input[name="shifts"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['logout_close_shift'] = array(
    '#title' => t('Close shift when loggin out'),
    '#description' => t('When a user logout his shift will be closed automaticly.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('logout_close_shift', FALSE),
    '#states' => array(
      'visible' => array(
        ':input[name="shifts"]' => array('checked' => TRUE),
      ),
    ),
  );

  $handlers = office_people_shift_handler_info();
  $options = array();

  foreach ($handlers as $handler_key => $plugin) {
    $handler = office_people_shift_handler($handler_key);
    if (empty($handler)) {
      continue;
    }

    if (method_exists($handler, 'handler_settings')) {
      $settings[$handler_key] = $handler->handler_settings();
    }
    $options[$handler_key] = $handler->title . ' - ' . $handler->description;
  }

  $form['shifts_handler'] = array(
    '#type' => 'select',
    '#title' => t('Shift handler'),
    '#description' => t('For calculating the Profit of the employee from the shift.'),
    '#default_value' => variable_get('shifts_handler', ''),
    '#options' => array('-1' => t('-- Select an handler --')) + $options,
    '#states' => array(
      'visible' => array(
        ':input[name="shifts"]' => array('checked' => TRUE),
      ),
    ),
    '#ajax' => array(
      'callback' => 'office_people_change_handler_settings',
      'wrapper' => 'handler_settings',
      'replace' => 'replace',
    ),
  );


  $handler = !empty($form_state['values']['shifts_handler']) ? $form_state['values']['shifts_handler'] : variable_get('shifts_handler');
  $form_state['rebuild'] = TRUE;

  $form['handler_settings'] = array(
    '#prefix' => '<div id="handler_settings">',
    '#suffix' => '</div>',
  );

  if (!empty($settings[$handler])) {
    $form['handler_settings'] += $settings[$handler];

    // Set defualt value.
    foreach (array_keys($settings[$handler]) as $key) {
      $form['handler_settings'][$key] += array(
        '#default_value' => variable_get($key),
        '#states' => array(
          'visible' => array(
            ':input[name="shifts"]' => array('checked' => TRUE),
          ),
        ),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Ajax callback - return the handler settings that you choses.
 */
function office_people_change_handler_settings($form, $form_sate) {
  return $form['handler_settings'];
}


/**
 * Function for generate user settings.
 */
function office_people_user_settings($form, $form_state) {
  $form = array();
  $account = office_user_init(NULL, 'wrapper');
  $settings = new entitySettings('user', $account->getIdentifier());

  // Push notifications settings area.
  $form['notifications'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notifications'),
    '#collapsible' => TRUE,

    'email' => array(
      '#type' => 'checkbox',
      '#title' => t('Email'),
      '#description' => t('Define if you want to receive email notifications'),
      '#default_value' => !is_object($settings) ? '' : $settings->email,
      '#access' => variable_get('office_people_notifications_email', FALSE),
    ),

    'push' => array(
      '#type' => 'checkbox',
      '#default_value' => !is_object($settings) ? '' : $settings->push,
      '#title' => t('Via push notifications(for IOS and android devices)'),
      '#description' => t('Send push notifications'),
      '#access' => variable_get('office_notifications_notifications', FALSE),
      '#disabled' => TRUE,
    ),

    'push_notifications_type' => array(
      '#title' => t('Push notification options'),
      '#description' => t('Choose the way you would like to send push notifications.'),
      '#default_value' => variable_get('office_notifications_push_types', array()),
      '#type' => 'checkboxes',
      '#options' => array(
        'android' => t('Push notification for android devices'),
        'ios' => t('Push notification for IOS devices'),
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="push"]' => array('checked' => TRUE),
        ),
      ),
      '#access' => variable_get('office_notifications_notifications', FALSE),
    )
  );

  if (!variable_get('office_people_notifications_email', FALSE) || variable_get('office_notifications_notifications', FALSE)) {
    $form['notifications']['#access'] = FALSE;
  }

  // Feilds settings.
  $fields = _office_people_return_field();
  $properties = $account->getPropertyInfo();

  // Adding phone field set.
  $form['phones'] = array(
    '#type' => 'fieldset',
    '#title' => t('Phone numbers'),
    '#weight' => '10',
  );

  // Run over the fields and build the form elements.
  foreach ($fields as $field) {
    if ($field == 'field_phone_number') {
      $phones = $account->field_phone_number->value();
      $phones[] = '';

      foreach ($phones as $i => $phone) {
        $form['phones'][$field . '[' . $i . ']'] = array(
          '#type' => 'textfield',
          '#title' => t('@label #@number', array('@label' => $properties[$field]['label'], '@number' => $i + 1)),
          '#value' => !empty($form_state['input'][$field][$i]) ? $form_state['input'][$field][$i] : $phone,
        );
      }
    }
    else {
      $form[$field] = array(
        '#type' => 'textfield',
        '#title' => $properties[$field]['label'],
        '#value' => !empty($form_state['input'][$field]) ? $form_state['input'][$field] : $account->{$field}->value(),
      );
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  return $form;
}

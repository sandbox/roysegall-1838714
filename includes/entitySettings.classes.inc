<?php
/**
 * @file
 * File for containg office main classes.
 */

/**
 * Class for handling all that relate to the user setting field.
 *
 * This class is very easy to use. First load the user settings:
 *  $entitySettings = new entitySettings(1);
 *
 * We can easily change the values and add values:
 *  $entitySettings->foo = 'bar';
 *
 * And now we can save the changes:
 *  $entitySettings->save();
 *
 * In case that we want to clear all the user settings:
 *  $entitySettings->clear();
 */
class entitySettings {
  /**
   * The wrapper of the user
   */
  private $wrapper;

  /**
   * The settings object.
   */
  private $settings = array();

  /**
   * Log the errors during the class workaround.
   */
  private $logs = array();

  /**
   * Array to hold the on the fly variables.
   */
  private $list;

  /**
   * The entity type.
   */
  private $entity_type;

  /**
   * The field name.
   */
  private $field;

  /**
   * On construct, let's check first that we have a field to work with.
   *
   * @param $type
   *  The entity type.
   * @param $entity
   *  The entity which holds the entity settings field.
   * @param $field
   *  A specific name for the entity field settings.
   */
  public function __construct($type, $entity, $field = NULL) {
    // Get the field for the user entity.
    $this->entity_type = $type;
    $this->wrapper = entity_metadata_wrapper($type, $entity);

    if (!$field) {
      $fields = $this->discoverEntitySettingsField();
      $this->field = $fields[0];
    }
    else {
      $this->field = $field;
    }

    // Loading the data and un-serialize it.
    $this->settings = $this->wrapper->{$this->field}->value();

    if (!empty($this->settings)) {
      // Initialize settings as object variables.
      foreach ($this->settings as $key => $value) {
        $this->{$key} = $value;
      }
    }

    return $this;
  }

  /**
   * Get the entity settings fields.
   */
  public function discoverEntitySettingsField() {
    $fields = office_entity_settings_get_fields();

    return $fields[$this->entity_type][$this->wrapper->getBundle()];
  }

  /**
   * Set a value.
   */
  public function __set($name, $value) {
    $this->{$name} = $value;

    // Add the new variables to the track list.
    $this->list[$name] = $name;
  }

  /**
   * Get value.
   */
  public function __get($name) {
    if (!isset($this->{$name})) {
      $this->logs[] = t('Tried to access to a non existing value in the user settings');
    }
  }

  /**
   * Saving the data.
   */
  public function save() {
    $data = $this->settings;

    foreach ($this->list as $list) {
      $data[$list] = $this->{$list};
    }

    $this->wrapper->{$this->field}->set($data);
    $this->wrapper->save();
  }

  /**
   * Clear the object.
   */
  public function clear() {
    $this->wrapper->{$this->field}->set('');
    $this->wrapper->save();

    return $this;
  }

  /**
   * Return the class logs.
   */
  function logs() {
    return $this->logs;
  }
}

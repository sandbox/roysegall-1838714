<?php
/**
 * @file
 * Standart shift handler.
 */

class office_people_shift_handler_standart extends shiftHandler {

  /**
   * Initialize the data for the handler.
   */
  function __construct() {
    $this->title = t('Standard shift handler');
    $this->description = t('Calculate the shift\'s wage without any speical labor law');
  }

  /**
   * Executing.
   */
  function execute() {
    // Add fields.
    $this
      ->fields('start','start_time')
      ->fields('end','end_time');

    // Calculate the hours.
    $seconds = $this->fields['end'] - $this->fields['start'];
    if ($seconds < 0) {
      $seconds = 0;
    }
    $hours = $seconds / 3600;

    // Get the wage.
    $wage = $this->account->field_wage_per_hour->value();

    // Simple handler, without any rules.
    $this->Profit += $hours * $wage;

    return $this;
  }
}
